========================
django-oscar-cybersource
========================

|  |build| |coverage| |license| |kit| |format|

This package is to handle integration between django-oscar based e-commerce sites and `Cybersource Secure Acceptance Silent Order POST <http://apps.cybersource.com/library/documentation/dev_guides/Secure_Acceptance_SOP/Secure_Acceptance_SOP.pdf>`_.

**Full Documentation**: https://django-oscar-cybersource.readthedocs.io

.. |build| image:: https://gitlab.com/thelabnyc/django-oscar/django-oscar-cybersource/badges/master/pipeline.svg
    :target: https://gitlab.com/thelabnyc/django-oscar/django-oscar-cybersource/commits/master
.. |coverage| image:: https://gitlab.com/thelabnyc/django-oscar/django-oscar-cybersource/badges/master/coverage.svg
    :target: https://gitlab.com/thelabnyc/django-oscar/django-oscar-cybersource/commits/master
.. |license| image:: https://img.shields.io/pypi/l/django-oscar-cybersource.svg
    :target: https://pypi.python.org/pypi/django-oscar-cybersource
.. |kit| image:: https://badge.fury.io/py/django-oscar-cybersource.svg
    :target: https://pypi.python.org/pypi/django-oscar-cybersource
.. |format| image:: https://img.shields.io/pypi/format/django-oscar-cybersource.svg
    :target: https://pypi.python.org/pypi/django-oscar-cybersource
